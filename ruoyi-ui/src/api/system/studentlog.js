import request from '@/utils/request'

// 查询日志列表
export function listStudentlog(query) {
  return request({
    url: '/system/studentlog/list',
    method: 'get',
    params: query
  })
}

// 查询日志详细
export function getStudentlog(id) {
  return request({
    url: '/system/studentlog/' + id,
    method: 'get'
  })
}

// 新增日志
export function addStudentlog(data) {
  return request({
    url: '/system/studentlog',
    method: 'post',
    data: data
  })
}

// 修改日志
export function updateStudentlog(data) {
  return request({
    url: '/system/studentlog',
    method: 'put',
    data: data
  })
}

// 删除日志
export function delStudentlog(id) {
  return request({
    url: '/system/studentlog/' + id,
    method: 'delete'
  })
}
