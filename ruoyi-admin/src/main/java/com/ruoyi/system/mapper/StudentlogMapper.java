package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.Studentlog;

/**
 * 日志Mapper接口
 * 
 * @author haoxx
 * @date 2022-03-27
 */
public interface StudentlogMapper 
{
    /**
     * 查询日志
     * 
     * @param id 日志主键
     * @return 日志
     */
    public Studentlog selectStudentlogById(Long id);

    /**
     * 查询日志列表
     * 
     * @param studentlog 日志
     * @return 日志集合
     */
    public List<Studentlog> selectStudentlogList(Studentlog studentlog);

    /**
     * 新增日志
     * 
     * @param studentlog 日志
     * @return 结果
     */
    public int insertStudentlog(Studentlog studentlog);

    /**
     * 修改日志
     * 
     * @param studentlog 日志
     * @return 结果
     */
    public int updateStudentlog(Studentlog studentlog);

    /**
     * 删除日志
     * 
     * @param id 日志主键
     * @return 结果
     */
    public int deleteStudentlogById(Long id);

    /**
     * 批量删除日志
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteStudentlogByIds(Long[] ids);
}
