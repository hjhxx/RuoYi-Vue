package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.StudentlogMapper;
import com.ruoyi.system.domain.Studentlog;
import com.ruoyi.system.service.IStudentlogService;

/**
 * 日志Service业务层处理
 * 
 * @author haoxx
 * @date 2022-03-27
 */
@Service
public class StudentlogServiceImpl implements IStudentlogService 
{
    @Autowired
    private StudentlogMapper studentlogMapper;

    /**
     * 查询日志
     * 
     * @param id 日志主键
     * @return 日志
     */
    @Override
    public Studentlog selectStudentlogById(Long id)
    {
        return studentlogMapper.selectStudentlogById(id);
    }

    /**
     * 查询日志列表
     * 
     * @param studentlog 日志
     * @return 日志
     */
    @Override
    public List<Studentlog> selectStudentlogList(Studentlog studentlog)
    {
        return studentlogMapper.selectStudentlogList(studentlog);
    }

    /**
     * 新增日志
     * 
     * @param studentlog 日志
     * @return 结果
     */
    @Override
    public int insertStudentlog(Studentlog studentlog)
    {
        return studentlogMapper.insertStudentlog(studentlog);
    }

    /**
     * 修改日志
     * 
     * @param studentlog 日志
     * @return 结果
     */
    @Override
    public int updateStudentlog(Studentlog studentlog)
    {
        return studentlogMapper.updateStudentlog(studentlog);
    }

    /**
     * 批量删除日志
     * 
     * @param ids 需要删除的日志主键
     * @return 结果
     */
    @Override
    public int deleteStudentlogByIds(Long[] ids)
    {
        return studentlogMapper.deleteStudentlogByIds(ids);
    }

    /**
     * 删除日志信息
     * 
     * @param id 日志主键
     * @return 结果
     */
    @Override
    public int deleteStudentlogById(Long id)
    {
        return studentlogMapper.deleteStudentlogById(id);
    }
}
