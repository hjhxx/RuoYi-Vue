package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.Studentlog;
import com.ruoyi.system.service.IStudentlogService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 日志Controller
 * 
 * @author haoxx
 * @date 2022-03-27
 */
@RestController
@RequestMapping("/system/studentlog")
public class StudentlogController extends BaseController
{
    @Autowired
    private IStudentlogService studentlogService;

    /**
     * 查询日志列表
     */
    @PreAuthorize("@ss.hasPermi('system:studentlog:list')")
    @GetMapping("/list")
    public TableDataInfo list(Studentlog studentlog)
    {
        startPage();
        List<Studentlog> list = studentlogService.selectStudentlogList(studentlog);
        return getDataTable(list);
    }

    /**
     * 导出日志列表
     */
    @PreAuthorize("@ss.hasPermi('system:studentlog:export')")
    @Log(title = "日志", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Studentlog studentlog)
    {
        List<Studentlog> list = studentlogService.selectStudentlogList(studentlog);
        ExcelUtil<Studentlog> util = new ExcelUtil<Studentlog>(Studentlog.class);
        util.exportExcel(response, list, "日志数据");
    }

    /**
     * 获取日志详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:studentlog:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(studentlogService.selectStudentlogById(id));
    }

    /**
     * 新增日志
     */
    @PreAuthorize("@ss.hasPermi('system:studentlog:add')")
    @Log(title = "日志", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Studentlog studentlog)
    {
        return toAjax(studentlogService.insertStudentlog(studentlog));
    }

    /**
     * 修改日志
     */
    @PreAuthorize("@ss.hasPermi('system:studentlog:edit')")
    @Log(title = "日志", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Studentlog studentlog)
    {
        return toAjax(studentlogService.updateStudentlog(studentlog));
    }

    /**
     * 删除日志
     */
    @PreAuthorize("@ss.hasPermi('system:studentlog:remove')")
    @Log(title = "日志", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(studentlogService.deleteStudentlogByIds(ids));
    }
}
