package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 日志对象 studentlog
 * 
 * @author haoxx
 * @date 2022-03-27
 */
public class Studentlog extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 班级id */
    @Excel(name = "班级id")
    private Long bjid;

    /** 班级名字 */
    @Excel(name = "班级名字")
    private String bjname;

    /** 姓名id */
    @Excel(name = "姓名id")
    private Long xsid;

    /** 姓名name */
    @Excel(name = "姓名name")
    private String xsname;

    /** 上课速度 */
    @Excel(name = "上课速度")
    private String velocity;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setBjid(Long bjid) 
    {
        this.bjid = bjid;
    }

    public Long getBjid() 
    {
        return bjid;
    }
    public void setBjname(String bjname) 
    {
        this.bjname = bjname;
    }

    public String getBjname() 
    {
        return bjname;
    }
    public void setXsid(Long xsid) 
    {
        this.xsid = xsid;
    }

    public Long getXsid() 
    {
        return xsid;
    }
    public void setXsname(String xsname) 
    {
        this.xsname = xsname;
    }

    public String getXsname() 
    {
        return xsname;
    }
    public void setVelocity(String velocity) 
    {
        this.velocity = velocity;
    }

    public String getVelocity() 
    {
        return velocity;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("bjid", getBjid())
            .append("bjname", getBjname())
            .append("xsid", getXsid())
            .append("xsname", getXsname())
            .append("velocity", getVelocity())
            .toString();
    }
}
